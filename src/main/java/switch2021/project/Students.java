package switch2021.project;

import java.util.List;
import java.util.Objects;

public class Students {
    private List<Student> students;

    // +accessor methods
    public Students(List<Student> students) {
        this.students = students;
    }

    public String getName(int index){
        return students.get(index).getName();

    }
    public int getScore(int index){
        return students.get(index).getScores();
    }
    public List<Student> getList(){
        return this.students;
    }






}

