package switch2021.project;

import java.util.Objects;

public class Student {


    private String name;
    private int score;

    // +accessor methods
    public Student(String name, int score) {
        this.name = name;
        this.score = score;
    }

    public int getScores() {
        return this.score;
    }

    public String getName() {
        return this.name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return score == student.score && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, score);
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", score=" + score +
                '}';
    }
}




