package switch2021.project;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class testingSortingTest {


    @Test
    void check() {
        Student student1 = new Student("antonio", 5);
        Student student2 = new Student("jose", 1);
        List<Student> list = new ArrayList<>();
        list.add(student1);
        list.add(student2);
        Students students = new Students(list);
        System.out.println(students.getName(0));
        System.out.println(students.getScore(0));
        System.out.println(students.getName(1));
        System.out.println(students.getScore(1));

        Collections.sort(students.getList(), Comparator.comparing(Student::getScores));
        System.out.println(students.getName(0));
        System.out.println(students.getScore(0));
        System.out.println(students.getName(1));
        System.out.println(students.getScore(1));

//        Collections.sort(student.getScores(), Comparator.comparing(Score::getGrade));

    }


}